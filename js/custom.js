
$(window).on('load', function () {
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	} else{
		$('body').addClass('web');
	};
	$('body').removeClass('loaded'); 
});
/* viewport width */
function viewport(){
	var e = window, 
		a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
/* viewport width */
$(function(){
	/* placeholder*/	   
	$('input, textarea').each(function(){
 		var placeholder = $(this).attr('placeholder');
 		$(this).focus(function(){ $(this).attr('placeholder', '');});
 		$(this).focusout(function(){			 
 			$(this).attr('placeholder', placeholder);  			
 		});
 	});
	/* placeholder*/

	$('.button-nav').click(function(){
		$(this).toggleClass('active'), 
		$('.main-nav-list').slideToggle(); 
		return false;
	});
	
	/* components */

	
	if($('.sidebar-select').length) {
		$('.sidebar-select').styler({
            //add\remove higher z index for parent and change arrow active
            onSelectOpened: function () {
                $(this).parent().css('z-index', '1001');
                $(this).siblings('.sidebar-arrow').addClass('sidebar-arrow__active');

            },
            onSelectClosed: function () {
                $(this).parent().css('z-index', '100');
                $(this).siblings('.sidebar-arrow').removeClass('sidebar-arrow__active');
            }
        });
	};
/*	if($('.fancybox').length) {
		$('.fancybox').fancybox({
			margin  : 10,
			padding  : 10
		});
	};*/
	if($('.slick-slider').length) {
		$('.slick-slider').slick({
			dots: true,
			speed: 300,
			slidesToShow: 3,
			slidesToScroll: 3,
			responsive: [
				{
				  breakpoint: 1200,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				  }
				},
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        dots: false
                    }
                }
			]
		});
	};
/*	if($('.scroll').length) {
		$(".scroll").mCustomScrollbar({
			axis:"x",
			theme:"dark-thin",
			autoExpandScrollbar:true,
			advanced:{autoExpandHorizontalScroll:true}
		});
	};*/

	
	/* component */
	
	

});

var handler = function(){
	
	var height_footer = $('footer').height();	
	var height_header = $('header').height();		
	//$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});
	
	
	var viewport_wid = viewport().width;
	var viewport_height = viewport().height;
	
	if (viewport_wid <= 991) {
		
	}
	
}
$(window).bind('load', handler);
$(window).bind('resize', handler);

//active sidebar point
$('.sidebar-point').on('click', function(){
    $('.sidebar-arrow', this).toggleClass('sidebar-arrow__active'); // change arrow transform
});

//active sumenu
$('.sidebar-point').on('click', function(){
    $(this).siblings('.sidebar-submenu').slideToggle() // show\hide submenu
});

//sidebar rating star click
$('.product-rating__star').on('click', function(){
	$(this).addClass('product-rating__star_active'); //active star
	$(this).prevAll().addClass('product-rating__star_active'); // add active star to all previous start
	$(this).nextAll().removeClass('product-rating__star_active'); // remove active star from all previous start
});